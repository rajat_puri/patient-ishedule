


import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import {
  FormBuilder,
  FormGroup,
  Validators,
  ReactiveFormsModule
} from "@angular/forms";
import {
  ToasterContainerComponent,
  ToasterService,
  ToasterConfig
} from "angular2-toaster";

import { DoctorService } from "../services/patient.service";



declare var $: any;
@Component({
  selector: "app-doctor-list",
  templateUrl: "./doctor-list.component.html",
  styleUrls: ["./doctor-list.component.css"],
  providers: [DoctorService]
})
export class DoctorListComponent implements OnInit {
  public config: ToasterConfig = new ToasterConfig({ limit: 1 });

  loading: boolean = false;
  allDoctors: any = [];
  constructor(
    private doctorService: DoctorService,
    private ts: ToasterService,
    private route: Router
  ) { }

  ngOnInit() {
    this.loading = true;

    this.getDoctorList();
  }

  getDoctorList() {
    this.doctorService.getDoctorList().subscribe(res => {
      console.log(res);
      this.loading = false;
      if (res.IsSuccess) {
        this.allDoctors = res.Data;
      } else {
        this.ts.pop("error", "", res.Message)
      }
    });
  }

  bookAppointment(doctor) {

    this.route.navigate(["/bookAppointmentSlot/" + doctor._id + "/" + doctor.firstName]);
  }
}
