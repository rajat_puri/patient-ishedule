import { Component, OnInit } from "@angular/core";
import { DoctorService } from "../services/patient.service";
import { Router, ActivatedRoute } from "@angular/router";


import {
  FormBuilder,
  FormGroup,
  Validators,
  ReactiveFormsModule
} from "@angular/forms";
import {
  ToasterContainerComponent,
  ToasterService,
  ToasterConfig
} from "angular2-toaster";
import { CookieService } from "../../landing/services/cookie.service";
import { moment } from 'ngx-bootstrap/chronos/test/chain';
import { ValueConverter } from '@angular/compiler/src/render3/view/template';

declare var $, swal;

@Component({
  selector: "app-book-appointment",
  templateUrl: "./book-appointment-slot.component.html",
  styleUrls: ["./book-appointment-slot.component.css"],
  providers: [DoctorService]
})

export class BookAppointmentSlotComponent implements OnInit {
  public config: ToasterConfig = new ToasterConfig({ limit: 1 });
  loading: boolean = false;
  dateOfAppointment: string = null;
  doctorId: string = "";
  doctorName: string = "";
  slotArray: any = [];
  dayName: string = "";
  emptySlot: boolean = false;
  minDate = new Date();
  constructor(
    private doctorService: DoctorService,
    private fb: FormBuilder,
    private ts: ToasterService,
    private cookieService: CookieService,
    private _route: ActivatedRoute,
    private router: Router

  ) { }


  ngOnInit() {
    this._route.params.subscribe(data => {
      console.log(data.id);
      this.doctorId = data.id;
      this.doctorName = data.name

    });


  }

  ngAfterViewInit() {

  }







  onDateValueChange(value: any): void {

    var todayTime = new Date(value);

    this.dayName = (value.toDateString())
    var month = todayTime.getMonth() + 1;
    var day = todayTime.getDate();
    var year = todayTime.getFullYear();
    var doa = year + "-" + month + "-" + day;

    //var doa = day + "/" + month + "/" + year;
    this.dateOfAppointment = doa


  }


  searchSlots() {
    this.emptySlot = false

    // this.doctorId = "5cbab271de3e275696acfbe0";
    this.doctorService.dateWiseAvailableSlots(this.doctorId, this.dateOfAppointment).subscribe(res => {
      console.log(res);
      if (res.IsSuccess) {

        this.slotArray = res.EmptySlots
        if (this.slotArray.length == 0)
          this.emptySlot = true


      } else {
        this.slotArray = [];

        this.ts.pop("error", "", res.Message)
      }


    });




  }




  bookAppointment(slotTime) {

    swal({
      title: "Are you sure wanna book appointment with Dr. " + this.doctorName.toUpperCase() + " on " + this.dayName + " at " + slotTime,
      type: "warning",
      showCancelButton: false,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "YES  !"
    }).then(result => {
      console.log(result);
      if (result) {

        let patData = {


          "service": "5cdd04223c8d0d098422bdeb",
          "dateOfAppointment": this.dateOfAppointment,
          "timeOfAppointment": slotTime,
          "doctorId": this.doctorId


        }

        this.doctorService.bookAppointment(patData).subscribe(res => {
          console.log(res);
          if (res.IsSuccess) {
            this.ts.pop("success", "", "Your appointment has been scheduled successfully")
          } else {
            this.ts.pop("error", "", res.Message);
          }
        });

      }
    });



  }

}
