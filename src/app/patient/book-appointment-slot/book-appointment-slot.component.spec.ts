import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookAppointmentSlotComponent } from './book-appointment-slot.component';

describe('BookAppointmentSlotComponent', () => {
  let component: BookAppointmentSlotComponent;
  let fixture: ComponentFixture<BookAppointmentSlotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookAppointmentSlotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookAppointmentSlotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
