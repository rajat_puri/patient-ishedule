import { Component, OnInit, AfterViewInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";

import {
  FormBuilder,
  FormGroup,
  Validators,
  ReactiveFormsModule
} from "@angular/forms";
import {
  ToasterContainerComponent,
  ToasterService,
  ToasterConfig
} from "angular2-toaster";

import { DoctorService } from "../services/patient.service";


declare var $: any;
@Component({
  selector: "app-doctor-profile-withoutlogin",
  templateUrl: "./doctor-profile.component.html",
  styleUrls: ["./doctor-profile.component.css"],
  providers: [DoctorService]
})
export class DoctorProfileComponent implements OnInit {
  public config: ToasterConfig = new ToasterConfig({ limit: 1 });

  loading: boolean = false;
  firstName: string = "";
  lastName: string = "";
  mobileNumber: string = "";
  pincode: string = "";
  email: string = "";
  address: string = "";
  doctorId: string = "";
  speciality: string = "";
  aboutme: string = "";
  designation: string = "";
  hospital: string = "";
  errorFirstName: boolean = false;
  errorLastName: boolean = false;
  formD: any;
  fileName: any;
  profilePicPath: string = "";

  allInsurances: any = [];
  allSpecialities: any = [];

  selectedInsurances: any = [];
  languages: any = [];

  boardCertification: string = "";
  eduAndTraining: string = "";
  awardAndPublication: string = "";
  npiNumber: string = "";
  officeLocationAddress: string = "";

  constructor(
    private doctorService: DoctorService,
    public _route: ActivatedRoute,
    private ts: ToasterService,
    private route: Router
  ) { }

  ngOnInit() {
    this._route.params.subscribe(data => {
      console.log(data);
      this.doctorId = data.id
      this.loading = true;
      this.getAllSpecialities();
      this.getDoctorPublicProfile(this.doctorId);
    });


  }

  getAllSpecialities() {
    this.doctorService.getAllSpecialities().subscribe(res => {
      console.log(res);
      if (res.IsSuccess) {
        this.allSpecialities = res.Data;
      } else {
        this.ts.pop("error", "", res.Message)
      }
    });
  }

  getDoctorPublicProfile(id) {
    this.doctorService.getDoctorPublicProfile(id).subscribe(res => {
      this.loading = false;
      console.log(res);
      if (res.IsSuccess) {
        this.firstName = res.Data.firstName;
        this.lastName = res.Data.lastName;
        this.mobileNumber = res.Data.mobileNumber;
        this.pincode = res.Data.pincode;
        this.email = res.Data.email;
        this.address = res.Data.address;
        this.profilePicPath = res.Data.profilePicPath;
        setTimeout(() => {
          this.speciality = this.getSpecialityName(res.Data.speciality);
        }, 200);

        this.aboutme = res.Data.aboutme;
        this.designation = res.Data.designation;
        this.hospital = res.Data.hospital;

        this.selectedInsurances = res.Data.insurances_ids;
        this.languages = res.Data.languagesSpeak;
        res.Data.languagesSpeak.forEach(language => {
          $("#inputTag").tagsinput("add", language);
        });

        res.Data.insurances_ids.forEach(insurance => {
          this.selectedInsurances.push(insurance._id);
        });

        (this.boardCertification = res.Data.boardCertification),
          (this.eduAndTraining = res.Data.eduAndTraining),
          (this.awardAndPublication = res.Data.awardAndPublication),
          (this.npiNumber = res.Data.npiNumber),
          (this.officeLocationAddress = res.Data.officeLocationAddress);
      } else {
        this.ts.pop("error", "", res.Message);
      }
    });
  }
  toggleMenu() {
    $("body").toggleClass("ls-toggle-menu");
  }
  getSpecialityName(id) {
    var foundSpecilaity = this.allSpecialities.find(function (element) {
      return element._id == id;
    });
    if (foundSpecilaity == undefined) return "";
    return (this.speciality = foundSpecilaity.displayName);
  }

  bookAppointment() {

    this.route.navigate(["patient/bookAppointmentSlot/" + this.doctorId + "/" + this.firstName]);
  }
}
