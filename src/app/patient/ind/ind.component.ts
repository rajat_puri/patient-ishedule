import { Component, OnInit } from '@angular/core';
declare var $: any;
declare var HeaderVideo: any;
import {
  ToasterContainerComponent,
  ToasterService,
  ToasterConfig
} from "angular2-toaster";

@Component({
  selector: 'app-ind',
  templateUrl: './ind.component.html',
  styleUrls: ['./ind.component.css']
})
export class IndComponent implements OnInit {
  public config: ToasterConfig = new ToasterConfig({ limit: 1 });

  loading: boolean = false;
  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    HeaderVideo.init({
      container: $('.header-video'),
      header: $('.header-video--media'),
      videoTrigger: $("#video-trigger"),
      autoPlayVideo: true
    });

    $('.owl-carousel').owlCarousel({
      loop: true,
      margin: 10,
      nav: true,
      responsive: {
        0: {
          items: 1
        },
        600: {
          items: 3
        },
        1000: {
          items: 5
        }
      }
    })
  }



}
