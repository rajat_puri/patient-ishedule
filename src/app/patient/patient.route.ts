import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { PatientComponent } from "./patient.component";
import { ListViewDoctorComponent } from "./list-view-doctor/list-view-doctor.component";
import { BookAppointmentComponent } from "./book-appointment/book-appointment.component";

import { PatientProfileComponent } from "./patient-profile/patient-profile.component";
import { EditPatientProfileComponent } from './edit-patient-profile/edit-patient-profile.component';
import { BookAppointmentSlotComponent } from './book-appointment-slot/book-appointment-slot.component';
import { DocumentsComponent } from './documents/documents.component';
import { ListAllDoctorsComponent } from './list-all-doctors/list-all-doctors.component';
import { DoctorProfileComponent } from './doctor-profile/doctor-profile.component';
import { ChatComponent } from './chat/chat.component';
import { AllCoupensComponent } from './all-coupens/all-coupens.component';
import { ViewCoupenComponent } from './view-coupen/view-coupen.component';

import { IndComponent } from './ind/ind.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { DoctorListComponent } from './doctor-list/doctor-list.component';
import { DoctordetailPageComponent } from './doctordetail-page/doctordetail-page.component';
import { DoctorComponent } from 'src/app/doctor/doctor.component';
import { BookingConfirmationComponent } from './booking-confirmation/booking-confirmation.component';
import { BookingDetailComponent } from './booking-detail/booking-detail.component';
import { CoupenComponent } from './coupen/coupen.component';
import { CoupenDetailComponent } from './coupen-detail/coupen-detail.component';



export const routes: Routes = [
  {
    path: "",
    component: PatientComponent,
    children: [
      { path: "", component: IndComponent, pathMatch: "full" },
      { path: "login", component: LoginComponent, pathMatch: "full" },
      { path: "coupen", component: CoupenComponent, pathMatch: "full" },
      { path: "coupenDetail/:id", component: CoupenDetailComponent, pathMatch: "full" },


      { path: "register", component: RegisterComponent, pathMatch: "full" },
      { path: "doctorList", component: DoctorListComponent, pathMatch: "full" },
      { path: "bookingDetail", component: BookingDetailComponent, pathMatch: "full" },
      { path: "bookingConfirmation", component: BookingConfirmationComponent, pathMatch: "full" },

      { path: "chat", component: ChatComponent, pathMatch: "full" },
      //  { path: "ind", component: IndComponent, pathMatch: "full" },

      { path: "allCoupens", component: AllCoupensComponent, pathMatch: "full" },
      { path: "viewCoupen/:id", component: ViewCoupenComponent, pathMatch: "full" },

      {
        path: "profile",
        component: PatientProfileComponent,
        pathMatch: "full"
      },
      {
        path: "doctorProfile/:id",
        component: DoctorProfileComponent,
        pathMatch: "full"
      },
      {
        path: "documents",
        component: DocumentsComponent,
        pathMatch: "full"
      },
      {
        path: "doctorList",
        component: ListAllDoctorsComponent,
        pathMatch: "full"
      },
      {
        path: "editPatientProfile",
        component: EditPatientProfileComponent,
        pathMatch: "full"
      },
      {
        path: "bookAppointment",
        component: BookAppointmentComponent,
        pathMatch: "full"
      },
      {
        path: "bookAppointmentSlot/:id/:name",
        component: BookAppointmentSlotComponent,
        pathMatch: "full"
      },
      {
        path: "doctorDetail/:id",
        component: DoctordetailPageComponent,
        pathMatch: "full"
      }
    ]
  }
];
