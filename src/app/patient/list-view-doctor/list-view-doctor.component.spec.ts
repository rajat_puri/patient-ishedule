import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListViewDoctorComponent } from './list-view-doctor.component';

describe('ListViewDoctorComponent', () => {
  let component: ListViewDoctorComponent;
  let fixture: ComponentFixture<ListViewDoctorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListViewDoctorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListViewDoctorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
