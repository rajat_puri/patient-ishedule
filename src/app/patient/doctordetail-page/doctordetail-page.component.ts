


import { Component, OnInit } from "@angular/core";
import { DoctorService } from "../services/patient.service";
import { Router, ActivatedRoute } from "@angular/router";


import {
  FormBuilder,
  FormGroup,
  Validators,
  ReactiveFormsModule
} from "@angular/forms";
import {
  ToasterContainerComponent,
  ToasterService,
  ToasterConfig
} from "angular2-toaster";
import { CookieService } from "../../landing/services/cookie.service";
import { moment } from 'ngx-bootstrap/chronos/test/chain';
import { ValueConverter } from '@angular/compiler/src/render3/view/template';

declare var $, swal;

@Component({
  selector: "app-book-appointment",
  templateUrl: "./doctordetail-page.component.html",
  styleUrls: ["./doctordetail-page.component.css"],
  providers: [DoctorService]
})

export class DoctordetailPageComponent implements OnInit {

  public config: ToasterConfig = new ToasterConfig({ limit: 1 });

  loading: boolean = false;
  firstName: string = "";
  lastName: string = "";
  mobileNumber: string = "";
  pincode: string = "";
  email: string = "";
  address: string = "";
  doctorId: string = "";
  speciality: string = "";
  aboutme: string = "";
  designation: string = "";
  hospital: string = "";
  errorFirstName: boolean = false;
  errorLastName: boolean = false;
  formD: any;
  fileName: any;
  profilePicPath: string = "";

  allInsurances: any = [];
  allSpecialities: any = [];

  selectedInsurances: any = [];
  languages: any = [];

  boardCertification: string = "";
  eduAndTraining: string = "";
  awardAndPublication: string = "";
  npiNumber: string = "";
  officeLocationAddress: string = "";



  dateOfAppointment: string = null;
  doctorName: string = "";
  slotArray: any = [];
  dayName: string = "";
  emptySlot: boolean = false;

  slotSelected: String = ""
  constructor(
    private doctorService: DoctorService,
    private fb: FormBuilder,
    private ts: ToasterService,
    private cookieService: CookieService,
    private _route: ActivatedRoute,
    private router: Router

  ) { }
  ngOnInit() {
    this._route.params.subscribe(data => {
      console.log(data);
      this.doctorId = data.id
      this.loading = true;
      // this.getAllSpecialities();
      this.getDoctorPublicProfile(this.doctorId);
    });


  }

  ngAfterViewInit() {
    var self = this;
    $("#booking_date").dateDropper({

    });

    $("#booking_date").change(function () {
      var todayTime = new Date($(this).val());

      this.dayName = (todayTime.toDateString())
      var month = todayTime.getMonth() + 1;
      var day = todayTime.getDate();
      var year = todayTime.getFullYear();
      var doa = year + "-" + month + "-" + day;

      self.dateOfAppointment = doa
      //  self.searchSlots()
    });

    this.getSlots()
  }


  getSlots() {
    var self = this;

    var todayTime = new Date($("#booking_date").val());

    this.dayName = (todayTime.toDateString())
    var month = todayTime.getMonth() + 1;
    var day = todayTime.getDate();
    var year = todayTime.getFullYear();
    var doa = year + "-" + month + "-" + day;

    self.dateOfAppointment = doa
    self.searchSlots()
  }


  getAllSpecialities() {
    this.doctorService.getAllSpecialities().subscribe(res => {
      console.log(res);
      if (res.IsSuccess) {
        this.allSpecialities = res.Data;
      } else {
        this.ts.pop("error", "", res.Message)
      }
    });
  }

  getDoctorPublicProfile(id) {
    this.doctorService.getDoctorPublicProfile(id).subscribe(res => {
      this.loading = false;
      console.log(res);

      if (res.IsSuccess) {
        this.firstName = res.Data.firstName;
        this.lastName = res.Data.lastName;
        this.mobileNumber = res.Data.mobileNumber;
        this.pincode = res.Data.pincode;
        this.email = res.Data.email;
        this.address = res.Data.address;
        this.profilePicPath = res.Data.profilePicPath;
        setTimeout(() => {
          if (res.Data.speciality_ids.length > 0)
            this.speciality = this.getSpecialityName(res.Data.speciality_ids[0]._id);

        }, 200);

        this.aboutme = res.Data.aboutme;
        this.designation = res.Data.designation;
        this.hospital = res.Data.hospital;

        this.selectedInsurances = res.Data.insurances_ids;
        this.languages = res.Data.languagesSpeak;
        // res.Data.languagesSpeak.forEach(language => {
        //   $("#inputTag").tagsinput("add", language);
        // });

        res.Data.insurances_ids.forEach(insurance => {
          this.selectedInsurances.push(insurance._id);
        });

        (this.boardCertification = res.Data.boardCertification),
          (this.eduAndTraining = res.Data.eduAndTraining),
          (this.awardAndPublication = res.Data.awardAndPublication),
          (this.npiNumber = res.Data.npiNumber),
          (this.officeLocationAddress = res.Data.officeLocationAddress);
      } else {
        this.ts.pop("error", "", res.Message);
      }
    });
  }
  toggleMenu() {
    $("body").toggleClass("ls-toggle-menu");
  }
  getSpecialityName(id) {
    var foundSpecilaity = this.allSpecialities.find(function (element) {
      return element._id == id;
    });
    if (foundSpecilaity == undefined) return "";
    return (this.speciality = foundSpecilaity.displayName);
  }







  onDateValueChange(value: any): void {

    var todayTime = new Date(value);

    this.dayName = (value.toDateString())
    var month = todayTime.getMonth() + 1;
    var day = todayTime.getDate();
    var year = todayTime.getFullYear();
    var doa = year + "-" + month + "-" + day;

    //var doa = day + "/" + month + "/" + year;
    this.dateOfAppointment = doa


  }


  searchSlots() {
    this.slotArray = [];

    this.emptySlot = false

    // this.doctorId = "5cbab271de3e275696acfbe0";
    this.doctorService.dateWiseAvailableSlots(this.doctorId, this.dateOfAppointment).subscribe(res => {
      console.log(res);
      if (res.IsSuccess) {
        this.slotArray = res.EmptySlots

        if (this.slotArray.length == 0)
          this.emptySlot =
            true



      } else {
        this.slotArray = [];

        //  this.ts.pop("error", "", res.Message)
      }

    });

    console.log(this.slotArray.length)

  }


  selectSlot(slot) {
    this.slotSelected = slot;
  }

  bookAppointment() {

    if (this.slotSelected == "") {
      this.ts.pop("error", "", "Please select an appointment time");
      return false
    }
    let patData = {


      "service": "5cdd04223c8d0d098422bdeb",
      "dateOfAppointment": this.dateOfAppointment,
      "timeOfAppointment": this.slotSelected,
      "doctorId": this.doctorId,
      "doctorName": this.firstName + ' ' + this.lastName


    }
    localStorage.setItem('patientData', JSON.stringify(patData));
    this.router.navigate(["/patient/bookingDetail"]);




  }

}

