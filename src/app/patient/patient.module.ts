import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";
import { routes } from "./patient.route";
import { ListViewDoctorComponent } from "./list-view-doctor/list-view-doctor.component";
import { PatientComponent } from "./patient.component";
import { PatientProfileComponent } from "./patient-profile/patient-profile.component";

import { BookAppointmentComponent } from "./book-appointment/book-appointment.component";

import { TopBarComponent } from "./top-bar/top-bar.component";
import { SideBarComponent } from "./side-bar/side-bar.component";

import { BsDatepickerModule } from "ngx-bootstrap/datepicker";
import { TimepickerModule } from "ngx-bootstrap/timepicker";

import { HttpModule } from "@angular/http";
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';

const config: SocketIoConfig = { url: 'http://api.ischedulenow.com/ ', options: {} };


import {
  FormBuilder,
  FormGroup,
  FormControl,
  FormsModule,
  ReactiveFormsModule,
  Validators,
  FormArray
} from "@angular/forms";

import {
  ToasterContainerComponent,
  ToasterService,
  ToasterConfig,
  ToasterModule
} from "angular2-toaster";

import { CookieService } from "../landing/services/cookie.service";
import { EditPatientProfileComponent } from './edit-patient-profile/edit-patient-profile.component';
import { BookAppointmentSlotComponent } from './book-appointment-slot/book-appointment-slot.component';
import { SearchNotesPipe } from './search-notes.pipe';
import { SearchChatPipe } from './search-chat.pipe';
import { NotesComponent } from './notes/notes.component';
import { DocumentsComponent } from './documents/documents.component';
import { ListAllDoctorsComponent } from './list-all-doctors/list-all-doctors.component';
import { DoctorProfileComponent } from './doctor-profile/doctor-profile.component';
import { ChatComponent } from './chat/chat.component';
import { AllCoupensComponent } from './all-coupens/all-coupens.component';
import { ViewCoupenComponent } from './view-coupen/view-coupen.component';
import { IndComponent } from './ind/ind.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { DoctorListComponent } from './doctor-list/doctor-list.component';
import { DoctordetailPageComponent } from './doctordetail-page/doctordetail-page.component';
import { BookingConfirmationComponent } from './booking-confirmation/booking-confirmation.component';
import { BookingDetailComponent } from './booking-detail/booking-detail.component';
import { CoupenComponent } from './coupen/coupen.component';
import { CoupenDetailComponent } from './coupen-detail/coupen-detail.component';


@NgModule({
  declarations: [
    ListViewDoctorComponent,
    PatientComponent,
    PatientProfileComponent,
    TopBarComponent,
    SideBarComponent,
    BookAppointmentComponent,
    EditPatientProfileComponent,
    BookAppointmentSlotComponent,
    SearchNotesPipe,
    SearchChatPipe,
    NotesComponent,
    DocumentsComponent,
    ListAllDoctorsComponent,
    DoctorProfileComponent,
    ChatComponent,
    AllCoupensComponent,
    ViewCoupenComponent,
    IndComponent,
    LoginComponent,
    RegisterComponent,
    HeaderComponent,
    FooterComponent,
    DoctorListComponent,
    DoctordetailPageComponent,
    BookingConfirmationComponent,
    BookingDetailComponent,
    CoupenComponent,
    CoupenDetailComponent,
  ],

  imports: [
    HttpModule,
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    ToasterModule,
    BsDatepickerModule,
    TimepickerModule.forRoot(),
    SocketIoModule.forRoot(config)

  ],
  providers: [CookieService]
})
export class PatientModule { }
