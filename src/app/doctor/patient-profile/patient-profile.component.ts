import { Component, OnInit } from "@angular/core";
import { DoctorService } from "../services/doctor.service";
import { Router, ActivatedRoute } from "@angular/router";
import * as filestack from 'filestack-js';
const client = filestack.init('AGqWW8kQNRqi122GGl1nvz');

import {
  FormBuilder,
  FormGroup,
  Validators,
  ReactiveFormsModule
} from "@angular/forms";
import {
  ToasterContainerComponent,
  ToasterService,
  ToasterConfig
} from "angular2-toaster";
import { Alert } from 'selenium-webdriver';



declare var $;

@Component({
  selector: "app-patient-profile",
  templateUrl: "./patient-profile.component.html",
  styleUrls: ["./patient-profile.component.css"],
  providers: [DoctorService]
})
export class PatientProfileComponent implements OnInit {
  public config: ToasterConfig = new ToasterConfig({ limit: 1 });

  firstName: string = "";
  middleName: string = "";
  profilePicPath: string = "";
  lastName: string = "";
  mobileNumber: string = "";
  pincode: string = "";
  email: string = "";
  address: string = "";
  title: string = "";
  description = "";
  patientId: string = ""
  billItems: any = [{}];
  prescriontionArray: any = [];
  invoiceTax: string = "";
  invoiceDiscount: string = "";
  invoiceNotes: string = "";
  invoiceDueDate: Date = new Date();
  loading: boolean = false;
  documents: any = [];


  invoiceDetails: any = [];

  documentUrl: string;
  documentType: string;
  documentStatus: string;

  constructor(
    private doctorService: DoctorService,
    private fb: FormBuilder,
    private ts: ToasterService,
    private _route: ActivatedRoute
  ) {
    this.loading = true;
  }

  ngOnInit() {
    this._route.params.subscribe(data => {
      console.log(data);
      this.patientId = data.id;
      this.getPatientProfile();
      this.getPatientPrescription();
      this.getPatientInvoice();
      this.getDocuments();
      setTimeout(() => {
        $("select").selectpicker();
      }, 500);

    });


  }

  addBillItem() {
    this.billItems.push({})
  }

  submitInvoice() {
    if (this.billItems.length == 0 || this.invoiceDueDate == null) {
      this.ts.pop("error", "", "Invoice cant be empty")
      return false;
    }
    let dataToSend = {

      "billItems": this.billItems,
      "subTotal": "40",
      "discount": this.invoiceDiscount,
      "tax": this.invoiceTax,
      "total": "39",
      "dueDate": this.invoiceDueDate,
      "patient_id": this.patientId,
      "notes": this.invoiceNotes

    }

    this.doctorService.addInvoice(dataToSend).subscribe(res => {
      console.log(res);
      if (res.IsSuccess) {
        this.ts.pop("success", "", "Invoice added successfully")
        this.billItems = [{}];
        this.invoiceNotes = "";
        this.invoiceDiscount = "";
        this.invoiceTax = "";
        this.invoiceDueDate = new Date()
        this.getPatientInvoice()
      } else {
        this.ts.pop("error", "", res.Message)
      }
    });
  }

  getPatientProfile() {
    this.doctorService.getPatientProfile(this.patientId).subscribe(res => {
      console.log(res);
      this.loading = false;
      if (res.IsSuccess) {
        this.firstName = res.Data.firstName;
        this.lastName = res.Data.lastName;
        this.mobileNumber = res.Data.mobileNumber;
        this.pincode = res.Data.pincode;
        this.email = res.Data.email;
        this.address = res.Data.address;
        this.profilePicPath = res.Data.profilePicPath
        setTimeout(() => {
          $("select").selectpicker();
        }, 500);
      } else {
        this.ts.pop("error", "", res.Message)
      }
    });
  }
  getInvoiceSubTotal(invoiceDetail) {
    let subTotal = 0;
    invoiceDetail.billItems.forEach(element => {
      subTotal = subTotal + parseInt(element.cost)
    });
    return subTotal

  }

  getInvoiceCost(invoiceDetail) {
    let subTotal = 0;
    invoiceDetail.billItems.forEach(element => {
      subTotal = subTotal + parseInt(element.cost)
    });
    let discountInInt = subTotal * parseInt(invoiceDetail.discount) / 100;
    let taxtInInt = subTotal * parseInt(invoiceDetail.tax) / 100;

    return (subTotal - discountInInt + taxtInInt)
  }

  getDocuments() {
    this.doctorService
      .getDocumentsForPatient(this.patientId)
      .subscribe(res => {
        if (res.IsSuccess) {

          this.documents = res.Data;
          setTimeout(() => {
            $("select").selectpicker();
          }, 500);
        } else {
          this.ts.pop("error", "", res.Message)
        }

      })

  }



  getPatientInvoice() {
    this.doctorService.getPatientInvoice(this.patientId).subscribe(res => {
      console.log(res);
      if (res.IsSuccess) {
        this.invoiceDetails = res.Data;
      } else {
        this.ts.pop("error", "", res.Message)
      }
    });
  }


  addPrescription() {
    let dataToSend = {

      "patientId": this.patientId,
      "title": this.title,
      "description": this.description

    }

    this.doctorService.addPatientPrescription(dataToSend).subscribe(res => {
      console.log(res);
      if (res.IsSuccess) {
        this.ts.pop("success", "", "Prescription Added");
        this.title = "";
        this.description = "";
        this.getPatientPrescription()

      } else {
        this.ts.pop("error", "", res.Message)

      }
    });
  }

  getPatientPrescription() {


    this.doctorService.getPatientPrescription(this.patientId).subscribe(res => {
      console.log(res);
      if (res.IsSuccess) {

        this.prescriontionArray = res.Data;
      } else {
        this.ts.pop("error", "", res.Message)

      }
    });
  }


  uploadDocument() {
    const options = {
      onFileUploadFinished: file => {
        // If you throw any error in this function it will reject the file selection.
        // The error message will be displayed to the user as an alert.
        console.log(file)
        this.documentUrl = file.url
      }
    };


    client.picker(options).open();
  }
  saveDocument() {

    if (this.description == "" || this.title == "" || this.documentType == "" || this.documentStatus == "") {

      // this.ts.pop("error", "", "please fill all required fields");
      return false;
    }

    let dataToSend = {
      "title": this.title,
      "description": this.description,
      "documentUrl": this.documentUrl,
      "documentType": this.documentType,
      "documentStatus": this.documentStatus,
      "isPrivate": false,
      "patient_id": this.patientId

    }

    this.doctorService
      .addDocToPatient(dataToSend)
      .subscribe(res => {
        if (res.IsSuccess) {
          this.ts.pop("success", "", "Document saved successfully");

          this.description = "";
          this.title = "";
          this.documentUrl = "";
          this.documentType = "";
          this.documentStatus = ""
          this.documentUrl = "";
          this.getDocuments();


        } else {
          this.ts.pop("error", "", res.Message)
        }

      })


  }
}
